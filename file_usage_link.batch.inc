<?php
/**
 * @file
 * Batch operations for File Usage Link.
 */

/**
 * Gets batch array for rebuilding file usage for all entities.
 */
function _file_usage_link_rebuild_batch() {
  $batch = array(
    'title' => t('Rebuilding file usage based on links in content'),
    'file' => drupal_get_path('module', 'file_usage_link') . '/file_usage_link.batch.inc',
    'finished' => '_file_usage_link_rebuild_finished',
    'operations' => array(),
  );

  // Find all entity type and bundle combinations to rebuild.
  $rebuild_entity_bundles = array();
  foreach (field_info_field_map() as $field_name => $field_map) {
    foreach ($field_map['bundles'] as $entity_type => $bundles) {
      foreach ($bundles as $bundle) {
        if ($field_map['type'] == 'link_field') {
          $rebuild_entity_bundles[$entity_type][$bundle] = TRUE;
        }
        else {
          $field_info_instance = field_info_instance($entity_type, $field_name, $bundle);
          if (!empty($field_info_instance['settings']['text_processing'])) {
            $rebuild_entity_bundles[$entity_type][$bundle] = TRUE;
          }
        }
      }
    }
  }

  // First, we clear all usage.
  $batch['operations'][] = array('_file_usage_link_rebuild_clear_operation', array());

  // Add an operation for each entity type.
  foreach ($rebuild_entity_bundles as $entity_type => $bundles) {
    $batch['operations'][] = array('_file_usage_link_rebuild_entity_bundle_operation', array($entity_type, array_keys($bundles)));
  }

  return $batch;
}

/**
 * Batch operation to clear all file usage from this module.
 */
function _file_usage_link_rebuild_clear_operation(&$context) {
  db_delete('file_usage')
    ->condition('module', 'file_usage_link')
    ->execute();
}

/**
 * Batch operation for rebuilding file usage for all entities of a type.
 */
function _file_usage_link_rebuild_entity_bundle_operation($entity_type, $bundles, &$context) {
  // Generate the list of ids at the beginning.
  if (!isset($context['sandbox']['id_type'])) {
    $entity_info = entity_get_info($entity_type);
    $context['message'] = t('Rebuilding file usage for %entity_type', array('%entity_type' => $entity_info['label']));
    $context['sandbox']['id_type'] = isset($entity_info['revision table']) && isset($entity_info['entity keys']['revision']) ? 'revision' : 'entity';

    // Query all entity ids or revision ids for the given entity type and bundle.
    $query = db_select($entity_info['base table'], 'e');
    if (!empty($entity_info['entity keys']['bundle']) && !empty($bundles)) {
      $query->condition('e.' . $entity_info['entity keys']['bundle'], $bundles, 'IN');
    }
    if ($context['sandbox']['id_type'] == 'entity') {
      $query->addField('e', $entity_info['entity keys']['id'], 'entity_id');
    }
    else {
      $query->join($entity_info['revision table'], 'r', 'e.' . $entity_info['entity keys']['id'] . ' = r.' . $entity_info['entity keys']['id']);
      $query->addField('r', $entity_info['entity keys']['revision'], 'revision_id');
    }
    $context['sandbox']['ids'] = $query->execute()->fetchCol();
    $context['results'][$entity_type] = count($context['sandbox']['ids']);
  }

  // Process 10 entities or revisions at once.
  $count = 0;
  while ($count++ < 10) {
    $id = array_shift($context['sandbox']['ids']);
    if (empty($id)) {
      continue;
    }

    if ($context['sandbox']['id_type'] == 'entity') {
      $entities = entity_load($entity_type, array($id));
      $entity = reset($entities);
    }
    else {
      $entity = _file_usage_link_entity_load_revision($entity_type, $id);
    }
    if (empty($entity)) {
      continue;
    }

    list ($entity_id,,) = entity_extract_ids($entity_type, $entity);

    // Add usage for this entity or revision.
    $files = file_usage_link_entity_field_count_files($entity_type, $entity);
    foreach ($files as $fid => $count) {
      if ($file = file_load($fid)) {
        file_usage_add($file, 'file_usage_link', $entity_type, $entity_id, $count);
      }
    }
  }

  // Mark as complete if we used all the ids.
  $context['finished'] = count($context['sandbox']['ids']) === 0;
}

/**
 * Batch 'finished' callback for rebuilding file usage.
 */
function _file_usage_link_rebuild_finished($success, $results, $operations) {
  drupal_set_message(t('Rebuilt file usage for %count entities/revisions.', array('%count' => array_sum($results))));
}
