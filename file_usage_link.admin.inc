<?php
/**
 * @file
 * Admin pages for File Usage Link module.
 */

/**
 * Page callback for File Usage Link admin form.
 */
function file_usage_link_rebuild_form($form, &$form_state) {
  $form['description'] = array(
    '#type' => 'markup',
    '#markup' => '<p>' . t('If you have lots of content before installing this module, or if something got out of sync, it can be useful to rebuild the file usage data for all existing content.') . '</p>',
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Rebuild'),
  );
  return $form;
}

/**
 * Form submission callback for file_usage_link_rebuild_form().
 */
function file_usage_link_rebuild_form_submit($form, &$form_state) {
  $batch = file_usage_link_rebuild_batch();
  batch_set($batch);
}
